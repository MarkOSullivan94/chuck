package dev.markos.template.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import dev.markos.template.data.viewstate.ChuckNorrisViewState
import dev.markos.template.providers.TestSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsInstanceOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ChuckNorrisViewModelTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var repository: ChuckNorrisRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun test_fetch_joke() {
        val joke = "Chuck Norris does not own a house. He walks into random houses and people move."
        every {
            repository.fetchJoke()
        } returns Single.just(joke)

        val vm = ChuckNorrisViewModel(
            repository = repository,
            schedulers = TestSchedulerProvider
        )

        assertThat(vm.joke.value, IsInstanceOf.instanceOf(ChuckNorrisViewState.Ready::class.java))
    }

    @After
    fun tearDown() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }
}