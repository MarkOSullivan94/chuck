package dev.markos.template.view

import android.animation.Animator
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Group
import androidx.core.view.isVisible
import com.airbnb.lottie.LottieAnimationView
import dagger.hilt.android.AndroidEntryPoint
import dev.markos.template.data.ChuckNorrisViewModel
import dev.markos.template.data.viewstate.ChuckNorrisViewState
import dev.markos.template.databinding.ActivityHomeBinding
import dev.markos.template.view.utils.SimpleAnimationListener
import timber.log.Timber

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private val chuckVm: ChuckNorrisViewModel by viewModels()
    private lateinit var binding: ActivityHomeBinding
    private lateinit var refresh: View
    private lateinit var refreshAnimation: LottieAnimationView
    private lateinit var joke: TextView
    private lateinit var readyGroup: Group
    private lateinit var errorGroup: Group
    private var currentState: ChuckNorrisViewState? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        readyGroup = binding.readyGroup
        errorGroup = binding.errorGroup
        joke = binding.joke
        refresh = binding.refreshBtnBackground
        refreshAnimation = binding.refreshBtnAnimation

        chuckVm.joke.observe(this, { state ->
            currentState = state
            when (state) {
                is ChuckNorrisViewState.Ready -> showJoke(state.joke)
                is ChuckNorrisViewState.Loading -> showLoading()
                is ChuckNorrisViewState.Error -> showError()
            }
        })

        refreshAnimation.addAnimatorListener(object : SimpleAnimationListener {
            // when the animation is finished
            // if we're still in loading state, replay the animation
            // otherwise show the relevant state
            override fun onAnimationEnd(animation: Animator?) {
                when (currentState) {
                    is ChuckNorrisViewState.Loading -> {
                        refreshAnimation.playAnimation()
                    }
                    is ChuckNorrisViewState.Ready -> {
                        readyGroup.isVisible = true
                    }
                    is ChuckNorrisViewState.Error -> {
                        errorGroup.isVisible = true
                    }
                }
            }
        })
    }

    private fun showJoke(joke: String) {
        refresh.setOnClickListener { refreshJoke() }
        readyGroup.isVisible = true
        this.joke.text = joke
    }

    private fun showLoading() {
        refreshAnimation.apply {
            playAnimation()
        }
    }

    private fun showError() {
        errorGroup.isVisible = true
        refresh.setOnClickListener { refreshJoke() }
    }

    // make sure the user cannot refresh twice
    // hide whatever state is visible
    // trigger request to fetch new joke
    private fun refreshJoke() {
        refresh.isClickable = false
        hideAll()
        chuckVm.fetchNewJoke()
    }

    private fun hideAll() {
        readyGroup.isVisible = false
        errorGroup.isVisible = false
    }
}
