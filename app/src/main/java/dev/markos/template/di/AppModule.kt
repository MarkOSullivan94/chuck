package dev.markos.template.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dev.markos.template.data.api.ChuckNorrisService
import dev.markos.template.data.api.ChuckNorrisService.Companion.CHUCK_BASE_URL
import dev.markos.template.providers.BaseSchedulerProvider
import dev.markos.template.providers.SchedulerProvider
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class AppModule {

    @Singleton
    @Provides
    fun providesChuckNorrisService(): ChuckNorrisService = Retrofit.Builder()
        .baseUrl(CHUCK_BASE_URL)
        .addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            )
        )
        .client(
            OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build()
        )
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(ChuckNorrisService::class.java)

    @Singleton
    @Provides
    fun provideScheduler(): BaseSchedulerProvider {
        return SchedulerProvider()
    }
}
