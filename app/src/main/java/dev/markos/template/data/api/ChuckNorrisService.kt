package dev.markos.template.data.api

import dev.markos.template.data.api.model.Response
import io.reactivex.Single
import retrofit2.http.GET

interface ChuckNorrisService {

    // http://api.icndb.com/jokes/random?exclude=[explicit]
    @GET("jokes/random?exclude=[explicit]")
    fun fetchJoke(): Single<Response>

    companion object {
        const val CHUCK_BASE_URL = "https://api.icndb.com/"
    }
}