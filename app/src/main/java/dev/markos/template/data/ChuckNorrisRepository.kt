package dev.markos.template.data

import android.os.Build
import android.text.Html
import dev.markos.template.data.api.ChuckNorrisService
import dev.markos.template.providers.BaseSchedulerProvider
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChuckNorrisRepository @Inject constructor(
    private val apiService: ChuckNorrisService,
    private val schedulers: BaseSchedulerProvider
) {
    fun fetchJoke(): Single<String> {
        return apiService.fetchJoke()
            .subscribeOn(schedulers.io())
            .map { response ->
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml(response.value.joke, Html.FROM_HTML_MODE_LEGACY).toString()
                } else {
                    Html.fromHtml(response.value.joke).toString()
                }
            }
    }
}
