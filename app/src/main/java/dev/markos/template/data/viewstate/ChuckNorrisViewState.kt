package dev.markos.template.data.viewstate

sealed class ChuckNorrisViewState {
    data class Ready(val joke: String) : ChuckNorrisViewState()
    object Loading : ChuckNorrisViewState()
    object Error : ChuckNorrisViewState()
}
