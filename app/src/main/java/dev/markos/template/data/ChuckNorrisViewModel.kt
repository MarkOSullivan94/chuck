package dev.markos.template.data

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dev.markos.template.data.viewstate.ChuckNorrisViewState
import dev.markos.template.providers.BaseSchedulerProvider
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class ChuckNorrisViewModel @ViewModelInject constructor(
    private val repository: ChuckNorrisRepository,
    private val schedulers: BaseSchedulerProvider
) : BaseViewModel() {

    private val _joke = MutableLiveData<ChuckNorrisViewState>()

    // immediately fetch joke when app is opened so user doesn't
    // have to click refresh button
    init {
        fetchNewJoke()
    }

    fun fetchNewJoke() {
        _joke.postValue(ChuckNorrisViewState.Loading)
        repository.fetchJoke()
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { joke -> _joke.postValue(ChuckNorrisViewState.Ready(joke)) },
                onError = {
                    Timber.e(it, "updateJoke:")
                    _joke.postValue(ChuckNorrisViewState.Error)
                }
            )
            .addTo(disposables)
    }

    val joke: LiveData<ChuckNorrisViewState>
        get() = _joke
}
