package dev.markos.template.data.api.model

import com.squareup.moshi.Json

data class Response(
    @field:Json(name = "type") val type: String,
    @field:Json(name = "value") val value: Value
)

data class Value(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "joke") val joke: String,
    @field:Json(name = "categories") val categories: List<String>
)