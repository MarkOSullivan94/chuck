# Chuck

<img src="https://i.imgur.com/O3IR6sS.png" width="260">

Libraries used include:

* Retrofit2 - Networking requests
* RxJava2 - Asynchronious requests & observable streams
* ConstraintLayout - Layouts
* Moshi - For parsing JSON API response
* ViewModel - Storing & managing UI related data
* LiveData - Lifecycle aware observable data
* Hilt - Dependency injection
* JUnit4 - Unit Testing

Also used:

* ViewBinding (Experimental but thought it'd be fun trying it out) - Code for easily interacting with views